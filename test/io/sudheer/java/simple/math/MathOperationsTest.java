package io.sudheer.java.simple.math;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MathOperationsTest {

    @Test
    public void addInt_shouldReturnSum_whenTwoIntegersAreProvided() {
        int result = MathOperations.addIntegers(1, 2);
        assertEquals(3, result);
    }

    @Test
    public void addInt_shouldReturnZero_whenZeroesAreProvided() {
        int result = MathOperations.addIntegers(0, 0);
        assertEquals(0, result);
    }

    @Test
    public void addInt_shouldReturnNegativeSum_whenNegativeIntegersAreProvided() {
        int result = MathOperations.addIntegers(-1, -2);
        assertEquals(-3, result);
    }

    @Test
    public void addString_shouldReturnConcatenatedString_whenTwoStringsAreProvided() {
        String result = MathOperations.addStrings("Hello", "World");
        assertEquals("'HelloWorld'", result);
    }

    @Test
    public void addString_shouldReturnSingleString_whenOneStringIsEmpty() {
        String result = MathOperations.addStrings("Hello", "");
        assertEquals("'Hello'", result);
    }

    @Test
    public void addString_shouldReturnEmptyString_whenBothStringsAreEmpty() {
        String result = MathOperations.addStrings("", "");
        assertEquals("''", result);
    }
}