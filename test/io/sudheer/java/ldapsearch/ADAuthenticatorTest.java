package io.sudheer.java.ldapsearch.authenticator;

import org.junit.jupiter.api.Test;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import java.util.Hashtable;

import static org.junit.jupiter.api.Assertions.*;

class ADAuthenticatorTest {

    @Test
    void successfulAuthentication() {
        Hashtable<String, String> env = io.sudheer.java.ldapsearch.authenticator.ADAuthenticator.getStringHashtable();
        env.put(Context.SECURITY_PRINCIPAL, "validUser@yourdomain");
        env.put(Context.SECURITY_CREDENTIALS, "validPassword");

        assertDoesNotThrow(() -> io.sudheer.java.ldapsearch.authenticator.ADAuthenticator.main(new String[]{}));
    }

    @Test
    void failedAuthenticationDueToInvalidCredentials() {
        Hashtable<String, String> env = io.sudheer.java.ldapsearch.authenticator.ADAuthenticator.getStringHashtable();
        env.put(Context.SECURITY_PRINCIPAL, "invalidUser@yourdomain");
        env.put(Context.SECURITY_CREDENTIALS, "invalidPassword");

        assertThrows(AuthenticationException.class, () -> io.sudheer.java.ldapsearch.authenticator.ADAuthenticator.main(new String[]{}));
    }

    @Test
    void failedAuthenticationDueToNamingException() {
        Hashtable<String, String> env = io.sudheer.java.ldapsearch.authenticator.ADAuthenticator.getStringHashtable();
        env.put(Context.PROVIDER_URL, "invalidUrl");

        assertThrows(NamingException.class, () -> io.sudheer.java.ldapsearch.authenticator.ADAuthenticator.main(new String[]{}));
    }
}