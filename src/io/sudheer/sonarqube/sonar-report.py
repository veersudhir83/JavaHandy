import pandas as pd
import numpy as np
import base64
import re
from requests import get

import plotly
import plotly.offline as py
import plotly.graph_objs as go

services = ['ACCTS:master', 'CART:dev']

coverage = []
vulnerabilities = []
names = []
bugs = []
ncloc = []
duplicated_files = []
code_smells = []
sqale_index = []


def metrics(metric):
    for key in services:
        url = "https://sonar.internal.shutterfly.com/api/measures/component?metricKeys="
        s = url + metric + "&componentKey=" + key
        result = get(s, auth=('8d76d2fb852ae6b0c823da58f701eda0661ce943', '')).json()
        names.append(result['component']['name'])

        cov = 0
        vul = 0
        bug = 0
        nloc = 0
        code_smell = 0
        sqale_ind = 0
        for key in result['component']['measures']:
            if key['metric'] == 'coverage':
                    cov = key['value']
            elif key['metric'] == 'vulnerabilities':
                    vul = key['value']
            elif key['metric'] == 'bugs':
                   bug = key['value']
            elif key['metric'] == 'ncloc':
                   nloc = key['value']
            elif key['metric'] == 'code_smells':
                   code_smell = key['value']
            elif key['metric'] == 'sqale_index':
                   time = int(key['value'])
                   sqale_ind = int(time // (8 * 60))


        coverage.append(float(cov))
        vulnerabilities.append(int(vul))
        bugs.append(int(bug))
        ncloc.append(int(nloc))
        code_smells.append(float(code_smell))
        sqale_index.append(int(sqale_ind))

metrics("coverage,vulnerabilities,code_smells,bugs,ncloc,sqale_index")

df = pd.DataFrame({"Coverage": coverage,
                    "Bugs": bugs,
                    "Vulnerability": vulnerabilities,
                    "Code Smells": code_smells,
                    "Lines of code":ncloc,
                    "Technical Debt":sqale_index},
                    index = names)

def hover(hover_color="#ffff99"):
    return dict(selector="tr:hover",
                props=[("background-color", "%s" % hover_color)])

styles = [
    hover(),
    dict(selector="th", props=[("font-size", "100%"),
                               ("text-align", "left")]),
    dict(selector="caption", props=[("caption-side", "bottom")])
]

def highlight_vals(val, min=70.00, max=100, color='green'):
    if min < val < max:
        return 'background-color: %s' % color
    else:
        return ''

def highlight_bugs(val, min=50, max=800, color='red'):
    if min < val < max:
        return 'background-color: %s' % color
    else:
        return ''


def highlight_max(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    is_max = s == s.max()
    return ['background-color: yellow' if v else '' for v in is_max]



######################### PIE #######################################
projects = names
coverage = coverage
trace = go.Pie(labels = names, values = coverage,hole=.3)
data = [trace]
fig = go.Figure(data = data)
fig.update_layout(width=1200, height=1000)
fig.update_layout(
    width=1500,
    height=1000,
    title_text="Coverage",
    font=dict(
        size=20
    ),
    titlefont=dict(
        size=50
    ),
    autosize=False
    )
fig.write_image('pie'+'.png')

data = open('pie.png', 'rb').read() # read bytes from file
data_base64 = base64.b64encode(data)  # encode to base64 (bytes)
data_base64 = data_base64.decode()    # convert bytes to string

with open('filename.html', 'w') as f:
    f.write(df.style.applymap(highlight_vals, subset=['Coverage']).set_table_styles(styles).set_caption("Sonar Analysis").set_table_attributes("border=0.9").set_precision(2).render())
    f.write("\n"+"<p> Note : For complete report visit <a href=\"https://sonar.internal.shutterfly.com\" target=\"iframe_a\">SonarQube</a></p>")

######################  EMAIL NOTIFICATION #########################
from pathlib import Path
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.image import MIMEImage


report_file = open(Path("filename.html"))
FROM = "noreply@something.com"
TO = "v-sveeravalli@shutterfly.com"

message = MIMEMultipart("alternative")
message["Subject"] = "Sonar Qube Report"
message['From'] = FROM
message['To'] = TO

# Create the plain-text and HTML version of your message
text = """\
SonarQube
"""

###############################################
html_string = report_file.read()
part1 = MIMEText(text, "plain")
part2 = MIMEText(html_string, "html")

message.attach(part1)
message.attach(part2)

# Send the mail
server = smtplib.SMTP('smtp.tnt1-zone2.phl1',25)
server.sendmail(FROM, TO, message.as_string())
server.quit()
