package io.sudheer.java.ldapsearch.authenticator;

import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;
import java.util.logging.Logger;

public class ADAuthenticator {
    private static final Logger logger = Logger.getLogger(ADLookup.class.getName());

    public static void main(String[] args) {
        Hashtable<String, String> env = getStringHashtable();

        try {
            // Create initial context
            DirContext ctx = new InitialDirContext(env);

            // If we reach here, the user is authenticated
            logger.info("User authenticated successfully!");

            // Close the context
            ctx.close();
        } catch (AuthenticationException e) {
            // If we get an AuthenticationException, the user is not authenticated
            logger.info("User authentication failed!");
        } catch (NamingException e) {
            logger.info("Error Message: " + e.getMessage());
        }
    }

    static Hashtable<String, String> getStringHashtable() {
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://your_domain:389"); // replace your_domain with your domain name
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "username@yourdomain"); // replace username with the username you want to authenticate
        env.put(Context.SECURITY_CREDENTIALS, "password"); // replace password with the password you want to authenticate
        return env;
    }
}