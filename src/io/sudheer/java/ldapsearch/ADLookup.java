package io.sudheer.java.ldapsearch.authenticator;

import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;
import java.util.logging.Logger;

public class ADLookup {

    private static final Logger logger = Logger.getLogger(ADLookup.class.getName());
    public static void main(String[] args) {
        Hashtable<String, String> env = getStringHashtable();

        try {
            // Create initial context
            DirContext ctx = new InitialDirContext(env);

            // Search for user in Active Directory
            String userToLookup = "user"; // replace user with the user you want to look up
            String searchFilter = "(&(objectCategory=person)(sAMAccountName=" + userToLookup + "))";
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> results = ctx.search("DC=your_domain,DC=com", searchFilter, searchControls); // replace your_domain,com with your domain

            // Iterate over search results
            while (results.hasMoreElements()) {
                SearchResult result = results.nextElement();
                Attributes attributes = result.getAttributes();
                logger.info("User found: " + attributes.get("sAMAccountName"));
            }

            // Close the context
            ctx.close();
        } catch (NamingException e) {
            logger.info("Error Message: " + e.getMessage());
        }
    }

    private static Hashtable<String, String> getStringHashtable() {
        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, "ldap://your_domain:389"); // replace your_domain with your domain name
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "username@yourdomain"); // replace username with the username you want to authenticate
        env.put(Context.SECURITY_CREDENTIALS, "password"); // replace password with the password you want to authenticate
        return env;
    }
}
