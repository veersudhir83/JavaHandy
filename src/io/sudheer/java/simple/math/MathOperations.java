package io.sudheer.java.simple.math;

import java.util.logging.Logger;

public class MathOperations {
    private static final Logger logger = Logger.getLogger(MathOperations.class.getName());

    public static void main(String[] args) {
        logger.info("Hello world!");
    }

    // Write a method to add two strings and return the result
    public static String addStrings(String a, String b) {
        logger.info("Adding " + a + " and " + b);
        return "'" + a + b + "'";
    }

    // Write a method to add two integers and return the result
    public static int addIntegers(int a, int b) {
        logger.info("Adding " + a + " and " + b);
        return a + b;
    }
}