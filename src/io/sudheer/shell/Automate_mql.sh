
#!/bin/ksh

#R_File=MATRIX####-R

HOST=`uname -n`
MAILADD="`cat mailids.txt`"
App_path=`pwd`
RFILES_Path="../../../"
SETCTXT="run ~/scripts/setcontext.mql"

#Clear working files ** may include *gz in future, will decide
rm -f nohup.out.gz schema.out.* SCHEMA_logs.tar

#Prepare variable containint a list of Currently installed R-Files
SchemaList=`ls "${RFILES_Path}" | grep MATRIX`

clear
echo "Automate_mql processing: "

R_File_found="0"
Exit_Error="0"

#Test for # of arguments provided.
#If RFilename and Script file is to be used you will need to update the following few lines and Usage Statement section
if (test "$#" = "1" ) then
   R_File="${1}"
   #ematrixid="${2}"
   #ematrixpass="${3}"
   #ScriptFile="${2}"
   ScriptFile="scripts.txt"

   echo "Script ${ScriptFile} against ${R_File} via User ${ematrixid}  ${DATE}" | tee > schema.out

   for name in `echo ${SchemaList}` ; do
        if [ "${name}" == "${R_File}" ]; then
                R_File_found="1"
                break
        fi
        Exit_Error="1"
   done
else
   Exit_Error="1"
fi

if [ "${R_File_found}" == "1" ]; then

   if [ ! -f "${ScriptFile}" ]; then
        echo ""
        echo "ScriptFile = ${ScriptFile} does not exist"
        echo "Invalid execution"
        echo ""
        Exit_Error="1"

   else
      exec `vi -c ":wq!" ${ScriptFile} > /dev/null`
      echo >> schema.out
      echo "R-File ${R_File}" >> schema.out
      echo >> schema.out

      cat ${ScriptFile} | while read MQLCMD; do
        Comment=`echo ${MQLCMD} | tr -s ' ' | cut -c1 `
        if [ "${Comment}" != "#" ] && [ "${Comment}" != "" ]; then

                #   TxtStr=`echo ${MQLCMD} | tr -s " " "_"`  (Must be used if using matrix mail facility)
                TxtStr=`echo ${MQLCMD}`
                echo >> schema.out
                echo "Processing - ${MQLCMD}" >> schema.out
                echo "Started - `exec date`" >> schema.out
                echo >> schema.out
                nohup ../../mql -b ${R_File} -c "verb on; list server; start trans; ${SETCTXT}; ${MQLCMD}; commit trans;" >> schema.out 2>&1
                RtnCd=${?}
                echo "Return code = ${RtnCd}"
                if (test "0" = "${RtnCd}") then
                        echo "${MQLCMD} - completed."
                else
                        echo "${MQLCMD} - failed"
                        DATE=`date '+%m%d%y:%T'`

                        # /usr/local/bin/gzip -f ./*.log
                        gzip -f ./schema.out
                        gzip -f ./nohup.out
                        tar -cvf SCHEMA_logs.tar schema.out.gz nohup.out.gz

                        FILE=SCHEMA_logs.tar

                        (echo "*** Automate_mql Process aborted! - ${TxtStr} on $HOST ***

                        Automate_mql.sh script processing for $App_path failed
                        Aborted: $DATE
                        System : $HOST


                        Processing : ${TxtStr}
                        ";  uuencode $FILE $FILE) | mailx -r eopspdm@ps.ge.com  -s "Automate_mql Process aborted! - ${TxtStr} on $HOST at $DATE" $MAILADD

                #       echo "Due to Fail status process is exiting" >> schema.out
                        exit 1
                fi

                if [ `grep -l "Warning: #1500023: Transaction aborted" schema.out` > /dev/null ]; then
                        echo "${MQLCMD} - Transaction Aborted - Rollback ocurred"

                        DATE=`date '+%m%d%y:%T'`

                        # /usr/local/bin/gzip -f ./*.log
                        gzip -f ./schema.out
                        gzip -f ./nohup.out
                        tar -cvf SCHEMA_logs.tar schema.out.gz nohup.out.gz

                        FILE=SCHEMA_logs.tar

                        (echo "*** Automate_mql Process aborted! - ${TxtStr} on $HOST ***

                        Automate_mql.sh script processing for $App_path failed
                        Aborted: $DATE
                        System : $HOST


                        Processing : ${TxtStr}
                        ";  uuencode $FILE $FILE) | mailx -r eopspdm@ps.ge.com  -s "Automate_mql Process aborted! - ${TxtStr} on $HOST at $DATE" $MAILADD


                #       echo "Due to Abort status process is exiting" >> schema.out
                        exit 1
                fi

                sleep 3
        fi
      done

     RtnCd=${?}
     if (test "0" = "${RtnCd}") then
        echo
        echo "Automate_MQL Complete - `exec date`" >> schema.out
        echo

        DATE=`date '+%m%d%y:%T'`

        # /usr/local/bin/gzip -f ./*.log
        gzip -f ./schema.out
        gzip -f ./nohup.out
        tar -cvf SCHEMA_logs.tar schema.out.gz nohup.out.gz

        FILE=SCHEMA_logs.tar

        (echo "*** Automate_mql Process Completed! - ${TxtStr} on $HOST ***

        Automate_mql.sh script processing for $App_path completed.
        Completed : $DATE
        System    : $HOST


        Processed :

`cat ${ScriptFile}`
        ";  uuencode $FILE $FILE) | mailx -r eopspdm@ps.ge.com  -s "Automate_mql Process Completed! - Automate_mql.sh for $App_path on $HOST at $DATE" $MAILADD
     fi

   fi
fi

if [ "${Exit_Error}" == "1" ]; then
    echo ""
    echo "       ie: Automate_mql.sh MATRIX-R"
    echo ""
    echo " Entered => Automate_mql.sh 1-${R_File}"
    echo ""
    if [ "${R_File_found}" == "0" ]; then
        echo "            R_File  -> ${R_File} <- "
        echo "            Does not exist in ${RFILES_Path}"
        echo
    fi
    exit 1;
fi

exit 0
