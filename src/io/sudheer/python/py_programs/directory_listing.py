from flask import Flask, render_template_string, send_from_directory
import os

app = Flask(__name__)

# Directory to list files from
DIRECTORY = './'

@app.route('/')
def directory_listing():
    files = []
    for filename in os.listdir(DIRECTORY):
        path = os.path.join(DIRECTORY, filename)
        if os.path.isfile(path):
            files.append(('file', filename))
        elif os.path.isdir(path):
            files.append(('dir', filename))
    
    # HTML template for directory listing
    html_template = """
    <!doctype html>
    <html>
    <head>
        <title>Directory Listing</title>
    </head>
    <body>
        <h1>Directory Listing</h1>
        <ul>
            {% for type, name in files %}
                {% if type == 'dir' %}
                    <li><strong>{{ name }}/</strong></li>
                {% else %}
                    <li><a href="/files/{{ name }}">{{ name }}</a></li>
                {% endif %}
            {% endfor %}
        </ul>
    </body>
    </html>
    """
    
    return render_template_string(html_template, files=files)

@app.route('/files/<path:filename>')
def download_file(filename):
    return send_from_directory(DIRECTORY, filename, as_attachment=True)

if __name__ == '__main__':
    app.run(debug=True)

