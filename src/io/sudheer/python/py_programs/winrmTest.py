import winrm

host = 'xxx.yyy'
user = 'user'
password = ''

session = winrm.Session(host, auth=('{}@{}'.format(user,domain), password), transport='ntlm')

result = session.run_cmd('ipconfig', ['/all']) # To run command in cmd
print(result.std_out)

result = session.run_ps('Get-Acl') # To run Powershell block
print(result.std_out)
