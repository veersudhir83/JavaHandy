import pandas as pd
import git
from git import Repo
from datetime import datetime
from dateutil import tz
import os

# Load the CSV file
def load_csv(file_path):
    try:
        df = pd.read_csv(file_path)
        return df
    except Exception as e:
        print(f"Error loading CSV file: {e}")

# Function to get the Git diff
def get_git_diff(repo_path, source_branch, target_branch):
    repo = Repo(repo_path)
    diff = repo.git.diff(source_branch, target_branch, name_only=True)
    # return sum(int(line.split()[0]) for line in diff.splitlines())
    diff_lines = diff.splitlines()
    return len(diff_lines)
    

# Function to get the merge date
def get_merge_date(repo_path, source_branch, target_branch):
    repo = Repo(repo_path)
    merge_commit = repo.commit(target_branch).parents[0]
    merge_date = datetime.strptime(str(merge_commit.committed_datetime), '%Y-%m-%d %H:%M:%S%z')
    # return merge_date
    pst = tz.tzstr('US/Pacific')
    pst_date = merge_date.astimezone(pst)
    return pst_date

# Main function
def main():
    # Load the CSV file
    df = load_csv('input.csv')

    # Create a new CSV file for output
    output_file = 'output.csv'
    if os.path.exists(output_file):
        os.remove(output_file)

    # Open the output CSV file for writing
    with open(output_file, 'w') as f:
        # Write the header
        f.write('repo_name,source_branch,target_branch,lines_diff,last_merge_date\n')

        # Iterate over the rows in the input CSV file
        for index, row in df.iterrows():
            repo_name = row['repo_name']
            source_branch = row['source_branch']
            target_branch = row['target_branch']

            # Get the Git diff
            diff = get_git_diff(repo_name, source_branch, target_branch)

            # Get the merge date
            merge_date = get_merge_date(repo_name, source_branch, target_branch)

            # Write the data to the output CSV file
            f.write(f'{repo_name},{source_branch},{target_branch},{diff},{merge_date}\n')

    print(f"Output CSV file saved to {output_file}")

if __name__ == "__main__":
    main()
