#!/usr/bin/env python

import os
import subprocess
from datetime import datetime
import matplotlib.pyplot as plt
from git import Repo
import pandas as pd
import csv

def get_commit_history(repo_path, branch, start_date=None):
    repo = Repo(repo_path)
    branch_commits = list(repo.iter_commits(branch))
    commit_data = []

    for commit in branch_commits:
        commit_date = datetime.fromtimestamp(commit.committed_date)
        if start_date and commit_date < start_date:
            break
        total_lines_changed = commit.stats.total.get("lines", 0)
        # Ensure the total lines changed is within a reasonable range
        if total_lines_changed <= 100000:
            commit_data.append((commit_date, total_lines_changed))

    return commit_data

def plot_code_drift(repo_path, branch1, branch2, start_date, repo_name):
    # Parse the start date
    start_date = datetime.strptime(start_date, "%Y-%m-%d")

    # Get commit histories for both branches
    branch1_data = get_commit_history(repo_path, branch1, start_date)
    branch2_data = get_commit_history(repo_path, branch2, start_date)

    # Aggregate the data for plotting
    dates_branch1 = [data[0] for data in branch1_data]
    lines_branch1 = [data[1] for data in branch1_data]

    dates_branch2 = [data[0] for data in branch2_data]
    lines_branch2 = [data[1] for data in branch2_data]

    # Apply rolling average for smoothing
    window_size = 5
    lines_branch1_smoothed = pd.Series(lines_branch1).rolling(window=window_size, min_periods=1).mean().tolist()
    lines_branch2_smoothed = pd.Series(lines_branch2).rolling(window=window_size, min_periods=1).mean().tolist()

    # Plot the data
    plt.figure(figsize=(12, 6))

    plt.plot(dates_branch1, lines_branch1_smoothed, label=f"{branch1} Changes", marker='o', linestyle='-', linewidth=1)
    plt.plot(dates_branch2, lines_branch2_smoothed, label=f"{branch2} Changes", marker='x', linestyle='--', linewidth=1)

    # Add labels, title, and grid
    plt.xlabel("Timeline")
    plt.ylabel("Lines Changed")
    plt.title(f"Code Drift Between Branches {branch1} and {branch2} of {repo_name}")
    plt.legend()
    plt.grid(True, linestyle='--', alpha=0.6)

    # Rotate date labels for better visibility
    plt.xticks(rotation=45)
    plt.tight_layout()

    # Save the plot as an image
    output_file = f"code_drift_{repo_name}.png"
    plt.savefig(output_file, bbox_inches='tight')  # Ensure entire plot is saved
    print(f"Graph saved as {output_file}")

def read_repo_details(csv_path):
    repo_details = pd.read_csv(csv_path)
    return repo_details

def log_message(log_file, message):
    with open(log_file, "a") as log:
        log.write(f"{datetime.now()} - {message}\n")

def clone_or_update_repo(repo_url, repo_path, log_file):
    try:
        message = f"Checking availability of repository: {repo_url}"
        print(message)
        log_message(log_file, message)

        # Check if the remote repository is accessible
        result = subprocess.run(["git", "ls-remote", repo_url], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode != 0:
            error_message = f"Error: Remote repository {repo_url} is not accessible. Skipping..."
            log_message(log_file, error_message)
            print(error_message)
            return False

        # Handle existing directory
        if os.path.exists(repo_path):
            if os.path.isdir(repo_path) and os.path.exists(os.path.join(repo_path, ".git")):
                message = f"Fetching updates for existing repository: {repo_path}"
                print(message)
                log_message(log_file, message)
                repo = Repo(repo_path)
                repo.remote().fetch()
            else:
                error_message = f"Error: Directory {repo_path} exists but is not a valid Git repository. Skipping..."
                log_message(log_file, error_message)
                print(error_message)
                return False
        else:
            # Clone the repository if the directory does not exist
            message = f"Cloning repository: {repo_url} to {repo_path}"
            print(message)
            log_message(log_file, message)
            Repo.clone_from(repo_url, repo_path)

        log_message(log_file, f"Success: Repository {repo_path} is ready.")
        return True
    except Exception as e:
        log_message(log_file, f"Error: Failed to clone or update repository {repo_url}. Error: {e}")
        print(f"Error: {e}")
        return False

def checkout_branch(repo_path, branch, log_file):
    try:
        repo = Repo(repo_path)
        message = f"Checking out branch: {branch} in {repo_path}"
        print(message)
        log_message(log_file, message)

        # Check if the branch exists in the remote
        branches = [ref.name for ref in repo.remote().refs]
        remote_branch = f"origin/{branch}"

        if remote_branch not in branches:
            error_message = f"Error: Branch {branch} does not exist in repository {repo_path}. Skipping..."
            log_message(log_file, error_message)
            print(error_message)
            return

        # Stash untracked changes
        repo.git.stash('save', '--include-untracked', '--quiet')
        
        # Checkout and pull the branch
        repo.git.checkout(branch)
        repo.remote().fetch(branch)
        repo.git.pull()
        log_message(log_file, f"Success: Checked out branch {branch} in {repo_path}.")
    except Exception as e:
        log_message(log_file, f"Error: Failed to checkout branch {branch} in {repo_path}. Error: {e}")
        print(f"Error: {e}")


if __name__ == "__main__":
    git_root = "/Users/veersudhir83/repos/local/"  # Replace with your Git root path
    csv_path = os.path.join(git_root, "repo_details.csv")  # Path to the CSV file
    log_file = os.path.join(git_root, "repo_processing.log")  # Log file path
    start_date = "2024-10-01"  # Replace with your start date in YYYY-MM-DD format

    # Read repository details from CSV
    repo_details = read_repo_details(csv_path)

    for _, row in repo_details.iterrows():
        repo_name = row["Repo_Name"]
        branch1 = row["Source_Branch"]
        branch2 = row["Target_Branch"]
        to_be_processed = row["To_Be_Processed"]

        # Skip repositories not marked for processing
        if to_be_processed.strip().lower() != "yes":
            log_message(log_file, f"Skipping repository {repo_name} as it is marked 'No' for processing.")
            print(f"Skipping repository {repo_name} as it is marked 'No' for processing.")
            continue

        repo_url = f"git@github.com:veersudhir83/{repo_name}.git"  # Replace with your SSH Git server URL
        repo_path = os.path.join(git_root, repo_name)

        try:
            # Clone or update repository
            if not clone_or_update_repo(repo_url, repo_path, log_file):
                continue

            # Checkout branches
            checkout_branch(repo_path, branch1, log_file)
            checkout_branch(repo_path, branch2, log_file)

            print(f"Processing repository: {repo_name} (Branches: {branch1} vs {branch2})")
            log_message(log_file, f"Processing repository: {repo_name} (Branches: {branch1} vs {branch2})")

            # Plot code drift
            plot_code_drift(repo_path, branch1, branch2, start_date, repo_name)
        except Exception as e:
            log_message(log_file, f"Error: Failed to process repository {repo_name}. Error: {e}")
            print(f"Error: {e}. Skipping to next repository.")
