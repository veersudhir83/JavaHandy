import openpyxl
from openpyxl.utils import get_column_letter

# Function to generate Excel file for a given month
def generate_excel_file(month_name):
    # Create a new workbook
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = month_name

    # Sample data
    headers = ['ID', 'Name', 'Quantity', 'Price']
    data = [
        (1, 'Product A', 10, 15.50),
        (2, 'Product B', 20, 12.25),
        (3, 'Product C', 15, 10.75),
        (4, 'Product D', 25, 8.99),
        (5, 'Product E', 30, 20.00)
    ]

    # Write headers
    for col_idx, header in enumerate(headers, 1):
        cell = ws.cell(row=1, column=col_idx)
        cell.value = header

    # Write data
    for row_idx, row_data in enumerate(data, 2):
        for col_idx, value in enumerate(row_data, 1):
            cell = ws.cell(row=row_idx, column=col_idx)
            cell.value = value

    # Save workbook
    wb.save(f'./../input/{month_name}.xlsx')

# List of months
months = ['January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December']

# Generate Excel files for each month
for month in months:
    generate_excel_file(month)

