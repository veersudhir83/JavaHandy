from collections import Counter

urls = [
    "x.com",
    "y.com",
    "x.com"
]

# Count occurrences of each URL
url_counts = Counter(urls)

# Filter URLs with count greater than 1 (duplicates)
duplicates = {url: count for url, count in url_counts.items() if count > 1}

print("Duplicates:")
for url, count in duplicates.items():
    print(f"{url}: {count} occurrences")
