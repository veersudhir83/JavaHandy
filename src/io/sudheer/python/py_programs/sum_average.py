from openpyxl import load_workbook
import os

# Load the workbook
directory_path = './../output'
file_name = 'Merged_File.xlsx'

file_path = os.path.join(directory_path, file_name)
workbook = load_workbook(filename=file_path)

# Get the active sheet
sheet = workbook.active

# Initialize variables for sum and count
sum_values = 0
count = 0

# Iterate through Column D starting from the second row (index 2)
for row in sheet.iter_rows(min_row=2, values_only=True):
    cell_value = row[3]  # Column D is index 3 in Python (0-indexed)
    if isinstance(cell_value, (int, float)):
        sum_values += cell_value
        count += 1

# Calculate average
if count > 0:
    average = sum_values / count
else:
    average = 0

# Print sum and average
print(f"Sum of values in Column D (excluding header row): {round(sum_values,2)}")
print(f"Average of values in Column D (excluding header row): {round(average,2)}")

