#!/usr/bin/env python3
# Purpose: Pull system stats about CPU, Disk Usage etc...
# Requirements:
#     - shutil,psutil 
import shutil
import psutil

def check_disk_usage(disk):
    du = shutil.disk_usage(disk)
    free = du.free / du.total * 100
    return free > 20

def check_cpu_usage():
    cpu_use = psutil.cpu_percent(1)
    return cpu_use < 75

if not check_disk_usage("/") or not check_cpu_usage():
    print("Error !")
else:
    print("Everything is Ok !")
