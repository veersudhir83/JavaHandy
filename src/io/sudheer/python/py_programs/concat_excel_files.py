import os
from openpyxl import load_workbook, Workbook

# Function to concatenate .xlsx files into a single file
def concatenate_xlsx_files():
    directory_path='./../input/'

    # Get a list of all .xlsx files in the current directory
    files = [file for file in os.listdir(directory_path) if file.endswith('.xlsx')]
    
    # Create a new workbook for the merged data
    merged_workbook = Workbook()
    merged_sheet = merged_workbook.active

    # Iterate through each file
    for index, file in enumerate(files):
        workbook = load_workbook(filename=os.path.join(directory_path, file))
        sheet = workbook.active

        # If it's the first file, include the header row
        include_header = True if index == 0 else False

        # Iterate through rows and append to the merged sheet
        for row_index, row in enumerate(sheet.iter_rows(values_only=True), 1):
            # Check if it's the header row and if it should be included
            if row_index == 1 and not include_header:
                continue  # Skip the header row for subsequent files
            merged_sheet.append(row)

    return merged_workbook

# Function to save the merged workbook to a file
def save_merged_workbook(merged_workbook):
    output_file_name = 'Merged_File.xlsx'
    output_dir = os.path.join(os.path.dirname(output_file_name), './../output')
    os.makedirs(output_dir, exist_ok=True)
    output_path = os.path.join(output_dir, os.path.basename(output_file_name))
    merged_workbook.save(filename=output_path)
    print("Merged file saved successfully as 'Merged_File.xlsx'.")

# Main function
def main():
    merged_workbook = concatenate_xlsx_files()
    save_merged_workbook(merged_workbook)

if __name__ == "__main__":
    main()

