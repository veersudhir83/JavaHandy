#!/usr/bin/env python3
# Purpose: Processing Image Files
# Requirements:
#     - python3-pil package using : apt-get install python3-pil
import PIL.Image
image = PIL.Image.open('My Image.JPG')
print(" Image Size = ", image.size)
print(" Image Format = ", image.format)

