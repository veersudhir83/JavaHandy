#!/usr/bin/env python3
# Purpose: Mapping of website errors using dataframes 
# Requirements:
#   - pandas package via pip3 : pip3 install pandas

import pandas
visitors = [ 111, 222, 333, 444, 555 ]
errors = [ 12, 23, 34, 45, 56 ]
df = pandas.DataFrame( {"visitors": visitors, "errors": errors}, index=["Mon", "Tues", "Wed", "Thu", "Fri"] )
print("DataFrame = \n", df)
print("Mean for Errors =", df["errors"].mean())
print("Mean for Visitors =", df["visitors"].mean())

