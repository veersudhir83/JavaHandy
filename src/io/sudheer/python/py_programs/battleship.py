#!/usr/bin/env python

__author__ = "Sudheer Veeravalli <veersudhir83@gmail.com>"
__copyright__ = "2024. Sudheer Veeravalli. Free for general use and re-distribution"
__license__ = "Public Domain"
__version__ = "1.1"

from random import randint

board = []

# Initializes the board
for x in range(5):
    board.append(["O"] * 5)

# prints the initial board
def show_battlefield(board):
    print("----------")
    for row in board:
        print(" ".join(row))
    print("----------")

# prints the board finally when success or fail
def show_battlefield_finally(board):
    show_battlefield(board)    
    print("X = Your Guess(es)")
    print("* = My Battleship")


show_battlefield(board)

ship_row = randint(1, len(board))
ship_col = randint(1, len(board))

# Give a max of 3 chances for the player to guess the battleship location
print("Try to sink my battleship by guess my coordinates")
for turn in range(1, 4):
    print("Turn", turn, "of 3")
    print("-----------")
    guess_row = int(input("Guess Row: "))
    guess_col = int(input("Guess Col: "))

    if guess_row == ship_row and guess_col == ship_col:
        print("Congratulations! You sunk my battleship!")
        board[ship_row - 1][ship_col - 1] = "*"
        show_battlefield_finally(board)
        break
    else:
        if guess_row not in range(6) or guess_col not in range(6):
            print("Oops, that's not even in the ocean.")
        elif board[guess_row - 1][guess_col - 1] == "X":
            print("You guessed that one already.")
        else:
            print("You missed my battleship!")
            board[guess_row - 1][guess_col - 1] = "X"
        if turn >= 3:
            print("Game Over!! You Lost !!")
            board[ship_row - 1][ship_col - 1] = "*"
            show_battlefield_finally(board)
